﻿using PatternsHomeWork.Base;
using Range = PatternsHomeWork.Base.Range;

namespace PatternsHomeWork;

class Program
{
    static void Main(string[] args)
    {
        Prototype3 proto3 = new Prototype3(new Range(2, 3), new PriceData(10, 100));

        Console.WriteLine($"{proto3.Print()} : (Deep Cloned){proto3.DeepClone().Print()}");
        Console.WriteLine($"{proto3.Print()} : (Shallow Cloned){((Prototype3)proto3.Clone()).Print()}");

        Prototype1 prototype1 = new Prototype1(10);
        Console.WriteLine($"{prototype1.Print()} : (Deep Cloned){prototype1.DeepClone().Print()}");
        Console.WriteLine($"{prototype1.Print()} : (Shallow Cloned){((Prototype1)prototype1.Clone()).Print()}");
        
        Prototype2 prototype2 = new Prototype2("user","Vasya","Pupkin");
        Console.WriteLine($"{prototype2.Print()} : (Deep Cloned){prototype2.DeepClone().Print()}");
        Console.WriteLine($"{prototype2.Print()} : (Shallow Cloned){((Prototype2)prototype2.Clone()).Print()}");
    }

  
}