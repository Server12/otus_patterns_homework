namespace PatternsHomeWork.Base;

public readonly struct Range
{
    public readonly float Min;
    public readonly float Max;

    public Range(float min, float max)
    {
        Min = min;
        Max = max;
    }

    public override string ToString()
    {
        return $"min: {Min} max: {Max}";
    }
}