namespace PatternsHomeWork.Base;

public class Prototype1 :PrototypeBase, IMyCloneable<Prototype1>
{
    public int Id { get; private set; }

    public Prototype1(int id)
    {
        Id = id;
    }

    public Prototype1 DeepClone()
    {
        return new Prototype1(Id);
    }

    public override string Print()
    {
        return $"{nameof(Prototype1)} hash:{GetHashCode()}";
    }
}