namespace PatternsHomeWork.Base;


public abstract class PrototypeBase:ICloneable
{
    public abstract string Print();

    public object Clone()
    {
        return this.MemberwiseClone();
    }
}