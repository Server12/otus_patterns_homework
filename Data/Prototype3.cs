namespace PatternsHomeWork.Base;

/// <summary>
/// Сдесь пример копированием PriceData глубоким и поверхностным копированием
/// </summary>
public class Prototype3 : PrototypeBase, IMyCloneable<Prototype3>
{
    public Prototype3(Range range, PriceData price)
    {
        Range = range;
        Price = price;
    }

    public Range Range { get; private set; }

    public PriceData Price { get; private set; }


    public Prototype3 DeepClone()
    {
        return new Prototype3(Range, Price.DeepClone());
    }

    public override string Print()
    {
        return $"{nameof(Prototype3)} hash:{GetHashCode()} priceDataHash:{Price.GetHashCode()}";
    }
}