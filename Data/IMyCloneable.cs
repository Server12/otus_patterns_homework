namespace PatternsHomeWork.Base;

public interface IMyCloneable<out T>where T:class
{
    T DeepClone();
}