namespace PatternsHomeWork.Base;

public class Prototype2 : PrototypeBase
{
    public readonly string UserName;
    public readonly string FirstName;
    public readonly string LastName;

    public Prototype2(string userName, string firstName, string lastName)
    {
        UserName = userName;
        FirstName = firstName;
        LastName = lastName;
    }

    public Prototype2 DeepClone()
    {
        return new Prototype2(UserName, FirstName, LastName);
    }

    public override string Print()
    {
        return $"{nameof(Prototype2)} hash:{GetHashCode()}";
    }
}