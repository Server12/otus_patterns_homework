namespace PatternsHomeWork.Base;

public class PriceData : IMyCloneable<PriceData>
{
    public int Price { get; private set; }

    public int Count { get; private set; }

    public PriceData(int count, int price)
    {
        Count = count;
        Price = price;
    }


    public PriceData DeepClone()
    {
        return new PriceData(Count, Price);
    }

    public object Clone()
    {
        return DeepClone();
    }

    public override string ToString()
    {
        return $"price: {Price} count: {Count}";
    }
}